# load in RVM environment
require 'rvm'
default_ruby = RVM.list_default
RVM.use! default_ruby
RVM.gemset_create app_name
RVM.gemset_use! app_name
run "echo \"#{default_ruby}\" >> .ruby-version"
run "echo \"#{app_name}\" >> .ruby-gemset"

gsub_file 'config/environments/production.rb', '# config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache', 'config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache'
gsub_file 'config/application.rb', '# config.time_zone = \'Central Time (US & Canada)\'', 'config.time_zone = \'UTC\''

create_file 'app/assets/stylesheets/application.css.scss' do <<-FILE
/*
 *= require_self
 */
@import 'bootstrap';
@import 'font-awesome';
FILE
end

remove_file "app/assets/stylesheets/application.css"

inject_into_file 'app/assets/javascripts/application.js', before: "//= require_tree ." do <<-FILE
//= require bootstrap
FILE
end

gem 'slim-rails'
gem 'jquery-ui-rails'
gem 'bootstrap-sass'
gem 'font-awesome-rails'

gem 'dalli'

gem 'uuidtools'
gem 'devise'
gem 'simple_form'
gem 'kaminari'

gem_group :development do
 gem 'quiet_assets'
 gem 'better_errors'
 gem 'binding_of_caller'
 gem 'bullet'
 gem 'brakeman', require: false	

end

run 'cp ./config/database.yml ./config/database.yml.skel'
run 'cp ./config/secrets.yml ./config/secrets.yml.skel'
run "cat << EOF > .gitignore
/.bundle
/db/*.sqlite3
/db/*.sqlite3-journal
/log/*.log
!/log/.keep
/tmp
database.yml
secrets.yml
doc/
*.swp
*~
.project
.idea
.secret
.DS_Store
EOF"


run 'bundle install'

generate 'simple_form:install --bootstrap'
generate 'controller StaticPages home'
route "root to: 'static_pages#home'"
generate 'devise:install'
generate 'devise User'
generate 'devise:views'


